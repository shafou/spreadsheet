$(document).on('turbolinks:load', function(){

  changeTotalColor();
  $(".hidden").hide();

  $('.form-control').on('blur', function(){
    var formSelector = $(this).closest('tr').children('form');
    makeAjaxCall(formSelector, 'POST');
  });

  $('btn').on('click', function(){
    $('form').on('ajax:success', function(e, data, status, xhr){
      alert('worked');
    })
    //$("#refresh").load(location.href + " #refresh");
  })

  function makeAjaxCall(selector, type){
    var getFormId = $(selector).attr('class').match(/\d+/).join();
    event.preventDefault();
    $.ajax({
      url: '/spreadsheets/'+ getFormId,
      type: type,
      data: $(selector).serialize(),
      success: function() {
        refreshPartial("#totals-wrapper", changeTotalColor);
      }
    });
  }

  function changeTotalColor(){
    var totalFinal = $("#totals-wrapper p:nth-last-of-type(1)").last();
    if (parseInt(totalFinal.html()) < 0) {
      totalFinal.addClass("text-danger");
    }
    else {
      totalFinal.addClass("text-success");
    }
  }

  function refreshPartial(selector, func){
    $(selector).load(location.href + " " + selector, function(){
      func();
    });
  }

})