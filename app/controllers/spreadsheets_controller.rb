class SpreadsheetsController < ApplicationController

  def index
    @spreadsheets = Spreadsheet.all
    @spreadsheet = Spreadsheet.new
  end

  def show
    @spreadsheet = Spreadsheet.find(params[:id])
  end

  def new
    @spreadsheet = Spreadsheet.new
  end

  def edit
    @spreadsheet = Spreadsheet.find(params[:id])
  end

  def create
    @spreadsheet = Spreadsheet.new(spreadsheet_params)
      if @spreadsheet.save
        redirect_to spreadsheets_path
      else
        render 'new'
      end
  end

  def update
  	@spreadsheet = Spreadsheet.find(params[:id])
      if @spreadsheet.update(spreadsheet_params)
      else
  		  render 'edit'
  	  end
  end

  def destroy
    @spreadsheet = Spreadsheet.find(params[:id])
    @spreadsheet.destroy
  
    redirect_to spreadsheets_path
  end

  def receita
    Spreadsheet.all.sum('renda_fixa_val + renda_extra_val')
  end

  def despesa
    Spreadsheet.all.sum('dividas_fixas_val + dividas_extras_val')
  end

  def total
    receita + despesa
  end

  private
    def spreadsheet_params
      params.require(:spreadsheet).permit(:dividas_fixas, :dividas_fixas_val,
      :dividas_extras, :dividas_extras_val, :renda_fixa_val, :renda_extra_val)
    end
end
