class RemovingExtraVals < ActiveRecord::Migration[6.0]
  def change
    remove_column :spreadsheets, :renda_fixa, :text
    remove_column :spreadsheets, :renda_extra, :text
  end
end
