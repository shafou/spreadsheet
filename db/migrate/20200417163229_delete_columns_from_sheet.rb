class DeleteColumnsFromSheet < ActiveRecord::Migration[6.0]
  def change
    remove_column :spreadsheets, :receita, :integer
    remove_column :spreadsheets, :despesa, :integer
  end
end
