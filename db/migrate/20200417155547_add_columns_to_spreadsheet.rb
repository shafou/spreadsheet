class AddColumnsToSpreadsheet < ActiveRecord::Migration[6.0]
  def change
    add_column :spreadsheets, :receita, :integer
    add_column :spreadsheets, :despesa, :integer
  end
end
