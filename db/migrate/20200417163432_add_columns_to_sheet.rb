class AddColumnsToSheet < ActiveRecord::Migration[6.0]
  def change
    add_column :spreadsheets, :tipo, :string
    add_column :spreadsheets, :nome, :string
    add_column :spreadsheets, :valor, :integer
    add_column :spreadsheets, :banco, :string
  end
end
