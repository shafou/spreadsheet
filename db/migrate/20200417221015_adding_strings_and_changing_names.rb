class AddingStringsAndChangingNames < ActiveRecord::Migration[6.0]
  def change
    change_column :spreadsheets, :dividas_fixas, 'text USING CAST(dividas_fixas AS text)'
    change_column :spreadsheets, :dividas_inesperadas, 'text USING CAST(dividas_inesperadas AS text)'
    change_column :spreadsheets, :renda_fixa, 'text USING CAST(renda_fixa AS text)'
    change_column :spreadsheets, :renda_extra, 'text USING CAST(renda_extra AS text)'
    add_column :spreadsheets, :dividas_fixas_val, :integer
    add_column :spreadsheets, :dividas_inesperadas_val, :integer
    add_column :spreadsheets, :renda_fixa_val, :integer
    add_column :spreadsheets, :renda_extra_val, :integer
  end
end
