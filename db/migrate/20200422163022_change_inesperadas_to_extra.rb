class ChangeInesperadasToExtra < ActiveRecord::Migration[6.0]
  def change
    rename_column :spreadsheets, :dividas_inesperadas, :dividas_extras
    rename_column :spreadsheets, :dividas_inesperadas_val, :dividas_extras_val
  end
end
