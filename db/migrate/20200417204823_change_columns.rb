class ChangeColumns < ActiveRecord::Migration[6.0]
  def change
    remove_column :spreadsheets, :tipo, :string
    remove_column :spreadsheets, :nome, :string
    remove_column :spreadsheets, :valor, :integer
    remove_column :spreadsheets, :banco, :string
    add_column :spreadsheets, :dividas_fixas, :string
    add_column :spreadsheets, :dividas_inesperadas, :string
    add_column :spreadsheets, :renda_fixa, :string
    add_column :spreadsheets, :renda_extra, :string
  end
end
