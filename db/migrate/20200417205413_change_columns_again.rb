class ChangeColumnsAgain < ActiveRecord::Migration[6.0]
  def change
    change_column :spreadsheets, :dividas_fixas, 'integer USING CAST(dividas_fixas AS integer)'
    change_column :spreadsheets, :dividas_inesperadas, 'integer USING CAST(dividas_inesperadas AS integer)'
    change_column :spreadsheets, :renda_fixa, 'integer USING CAST(renda_fixa AS integer)'
    change_column :spreadsheets, :renda_extra, 'integer USING CAST(renda_extra AS integer)'
  end
end
